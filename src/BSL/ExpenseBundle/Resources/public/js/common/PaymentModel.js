// src/BSL/ExpenseBundle/Resources/public/js/views/PaymentModel.js
/**
 * PaymentModel is a model for the business objects pertaining to expense.
 **/

var PaymentModel = function() {
    var self = this;

    self.amountPaid = ko.observable("");
    self.isPaid = ko.observable("");
    self.paymentDate = ko.observable("");
}