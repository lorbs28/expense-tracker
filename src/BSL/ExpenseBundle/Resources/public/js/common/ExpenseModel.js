// src/BSL/ExpenseBundle/Resources/public/js/views/ExpenseModel.js

/**
 * ExpenseModel is a model for the business objects pertaining to expense.
 **/

var ExpenseModel = function() {
    var self = this;

    /******
     * Declare JS variables;
     ******/


    /******
     * Declare observables
     ******/
    self.expenseName = ko.observable("");
    self.dueDate = ko.observable("");
    self.amountDue = ko.observable("");
    self.expenseId = ko.observable("");
    self.isNew = ko.observable(""); // default to new expense payment
    self.isEditingRecord = ko.observable("");
    self.payment = ko.observable();

    /******
     * Declare computed observables
     ******/

    /******
     * Declare functions
     ******/
    //Save a new expense to the database
    self.saveNewExpense = function() {
        var route;
        var data;

        self.isNew(false); // Set isNew to false, since we're saving the new expense it will no longer be new after the save.
        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_createExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                dataType: "JSON",
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE

                    self.expenseId(result.expenseId);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    };

    // Update an existing expense
    self.updateExpense = function() {
        var route;
        var data;

        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_updateExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                dateType: "JSON",
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    };

    // Remove an expense
    self.deleteExpense = function() {
        var route;
        var data;

        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_deleteExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    }

    // Get all expenses for user
    self.getAllExpensesForUser = function() {
        var route;

        route = Routing.generate("BSLExpenseBundle_getAllExpensesForUser");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    }


}
