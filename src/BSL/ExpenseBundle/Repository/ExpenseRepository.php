<?php
// src/BSL/ExpenseBundle/Repository/ExpenseRepository.php
namespace BSL\ExpenseBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ExpenseRepository
 * @package BSL\ExpenseBundle\Repository
 */
class ExpenseRepository extends EntityRepository
{
    public function findAllExpensesForUserId($userId)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('e')
            ->from('BSLExpenseBundle:Expense', 'e')
            ->where('e.userId = :userId')
            ->setParameter('userId', $userId);

        $q = $qb->getQuery();

        return $q->getResult();
    }
}