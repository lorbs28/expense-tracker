<?php
// src/BSL/ExpenseBundle/Controller/ExpenseController.php
namespace BSL\ExpenseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ExpenseController
 * @package BSL\ExpenseBundle\Controller
 */
class ExpenseController extends Controller
{
    /**
     * This controller function will render the index page for this controller.
     *
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getUser();

        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render("BSLExpenseBundle:Expense:index.html.twig", array(
                'user' => $user,
            ));
        }

        return $this->render("BSLExpenseBundle:Expense:index.html.twig");
    }

    /**
     * This controller function will update an expense record.
     *
     * @param Request $request
     * @return Response
     */
    public function updateExpenseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); // Get the entity manager
        $data = $request->request->get('jsonObj'); // Get the request object

        // TODO: Move below code into an "ExpenseManager" class
        $serializer = $this->container->get('serializer'); // Get the serializer
        $newExpense = $serializer->deserialize($data, 'BSL\ExpenseBundle\Entity\Expense', 'json'); // Deserialize the new expense info

        $id = $newExpense->getId(); // Get the expense id
        $currentExpense = $em->getRepository('BSLExpenseBundle:Expense')->find($id); // Find the old expense with the given id

        if ($id == $currentExpense->getId()) {
            // Set the new expense values into the current expense fields
            $currentExpense->setExpenseName($newExpense->getExpenseName()); // Update the expense name
            $currentExpense->setDueDate($newExpense->getDueDate()); // Update the due date
            $currentExpense->setAmountDue($newExpense->getAmountDue()); // Update the amount due

            $currentPayment = $currentExpense->getPayment(); // Get the current payment object that's associated with the current expense
            $newPayment = $newExpense->getPayment(); // Get the new payment object

            $currentPayment->setAmountPaid($newPayment->getAmountPaid()); // Update the amount paid
            $currentPayment->setIsPaid($newPayment->getIsPaid()); // Update the isPaid status

            $currentExpense->setPayment($currentPayment); // Associate the current, updated payment with the current, updated expense
            $currentPayment->setExpense($currentExpense); // Associate the current, updated expense with the current, updated payment
        }

        $em->flush(); // Flush that bad boy

        // Serialize our expense entity to send back to our KO viewmodel
        $expenseJson = $serializer->serialize($currentExpense, 'json');

        return new Response($expenseJson);
    }

    /**
     * This controller function will create an expense record.
     *
     * @param Request $request
     * @return Response
     */
    public function createExpenseAction(Request $request)
    {
        //  Get the necessary objects
        $em = $this->getDoctrine()->getManager(); // Get the entity manager
        $userId = $this->getUser()->getId(); // Get the user id
        $data = $request->request->get('jsonObj'); // Get the request object

        // TODO: Move below code into an "ExpenseManager" class
        $serializer = $this->container->get('serializer'); // Get the serializer
        $expense = $serializer->deserialize($data, 'BSL\ExpenseBundle\Entity\Expense', 'json'); // Deserialize incoming JSON object

        $payment = $expense->getPayment(); // Get the payment object from inside the expense object

        $expense->setUserId($userId); // Set the user id to the expense object

        $expense->setPayment($payment); // Set the payment object to the expense object
        $payment->setExpense($expense); // Set the expense object to the payment object

        $em->persist($expense); // Persist the expense object
        $em->persist($payment); // Persist the payment object
        $em->flush(); // Flush it!

        // Serialize our expense entity to send back to our KO viewmodel
        $expenseJson = $serializer->serialize($expense, 'json');

        return new Response($expenseJson);
    }

    /**
     * This controller function will delete an expense record.
     *
     * @param Request $request
     * @return Response
     */
    public function deleteExpenseAction(Request $request)
    {
        //  Get the necessary objects
        $em = $this->getDoctrine()->getManager(); // Get the entity manager
        $data = $request->request->get('jsonObj'); // Get the request object

        // TODO: Move below code into an "ExpenseManager" class
        $serializer = $this->container->get('serializer'); // Get the serializer
        $expense = $serializer->deserialize($data, 'BSL\ExpenseBundle\Entity\Expense', 'json'); // Deserialize incoming JSON object
        $expense = $em->merge($expense); // Let the entity manager start managing

        $em->remove($expense); // Remove the expense object, this will also remove the associated payment object.
        $em->flush(); // Flush flush baby.

        return new Response("Expense deleted."); // TODO: refactor with better response
    }

    public function getAllExpensesForUserAction()
    {
        // Get the necessary objects
        $em = $this->getDoctrine()->getManager(); // Get the entity manager
        $userId = $this->getUser()->getId(); // Get the user id
        $serializer = $this->container->get('serializer');

        $expenses = $em->getRepository('BSLExpenseBundle:Expense')->findAllExpensesForUserId($userId);

        $expenseJson = $serializer->serialize($expenses, 'json');

        return new Response($expenseJson);
    }


}