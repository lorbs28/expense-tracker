<?php

namespace BSL\ExpenseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\SerializerBundle\Annotation as JMS;

/**
 * Payment
 *
 * @ORM\Table(name="payment", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Payment
{
    /**
     * @var string
     *
     * @ORM\Column(name="amount_paid", type="decimal", precision=12, scale=2, nullable=false)
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("amountPaid")
     */
    private $amountPaid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_paid", type="boolean", nullable=false)
     *
     * @JMS\Type("boolean")
     * @JMS\SerializedName("isPaid")
     */
    private $isPaid;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \BSL\ExpenseBundle\Entity\Expense
     *
     * @ORM\OneToOne(targetEntity="Expense", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="expense_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $expense;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date", type="datetime", nullable=true)
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("paymentDate")
     */
    private $paymentDate;

    /**
     * Set amountPaid
     *
     * @param string $amountPaid
     * @return Payment
     */
    public function setAmountPaid($amountPaid)
    {
        $this->amountPaid = $amountPaid;

        return $this;
    }

    /**
     * Get amountPaid
     *
     * @return string 
     */
    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    /**
     * Set isPaid
     *
     * @param integer $isPaid
     * @return Payment
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return integer 
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expense
     *
     * @param \BSL\ExpenseBundle\Entity\Expense $expense
     * @return Payment
     */
    public function setExpense(\BSL\ExpenseBundle\Entity\Expense $expense = null)
    {
        $this->expense = $expense;

        return $this;
    }

    /**
     * Get expense
     *
     * @return \BSL\ExpenseBundle\Entity\Expense
     */
    public function getExpensePayment()
    {
        return $this->expense;
    }

    /**
     * Set paymentDate
     *
     * @param \DateTime $paymentDate
     * @return Payment
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime 
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setPaymentDateValue()
    {
        $date = $this->paymentDate;
        $isPaid = $this->isPaid;

        if ($isPaid && !is_object($date)) {
            $this->paymentDate = $this->convertDateStringToDateTime($date);
        } else if ($isPaid && is_object($date)) {
            $this->paymentDate = new \DateTime("now");
        } else if (!$isPaid) {
            $this->paymentDate = null;
        }

    }

    public function convertDateStringToDateTime($date)
    {
        return new \DateTime($date);
    }

}
