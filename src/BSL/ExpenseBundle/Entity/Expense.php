<?php

namespace BSL\ExpenseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\SerializerBundle\Annotation as JMS;

/**
 * Expense
 *
 * @ORM\Table(name="expense", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="payment_id_fk_idx", columns={"payment_id"}), @ORM\Index(name="user_id_fk_idx", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="BSL\ExpenseBundle\Repository\ExpenseRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Expense
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=false)
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("dueDate")
     */
    private $dueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_due", type="decimal", precision=12, scale=2, nullable=false)
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("amountDue")
     */
    private $amountDue;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Type("integer")
     * @JMS\SerializedName("expenseId")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @JMS\Type("integer")
     * @JMS\SerializedName("userId")
     */
    private $userId;

    /**
     * @var \BSL\ExpenseBundle\Entity\Payment
     *
     * @ORM\OneToOne(targetEntity="BSL\ExpenseBundle\Entity\Payment", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     *
     * @JMS\Type("\BSL\ExpenseBundle\Entity\Payment")
     * @JMS\SerializedName("payment")
     */
    private $payment;

    /**
     * @var string
     * @ORM\Column(name="expense_name", type="string", length=255, unique=false, nullable=false)
     *
     * @JMS\Type("string")
     * @JMS\SerializedName("expenseName")
     */
    private $expenseName;

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return \DateTime
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set amountDue
     *
     * @param string $amountDue
     * @return string
     */
    public function setAmountDue($amountDue)
    {
        $this->amountDue = $amountDue;

        return $this;
    }

    /**
     * Get amountDue
     *
     * @return string 
     */
    public function getAmountDue()
    {
        return $this->amountDue;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user id
     *
     * @param integer $userId
     * @return integer
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get user id
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set payment
     *
     * @param \BSL\ExpenseBundle\Entity\Payment $payment
     * @return ExpensePayment
     */
    public function setPayment(\BSL\ExpenseBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \BSL\ExpenseBundle\Entity\Payment 
     */
    public function getPayment()
    {
        return $this->payment;
    }


    /**
     * Set expenseName
     *
     * @param string $expenseName
     * @return ExpensePayment
     */
    public function setExpenseName($expenseName)
    {
        $this->expenseName = $expenseName;

        return $this;
    }

    /**
     * Get expenseName
     *
     * @return string 
     */
    public function getExpenseName()
    {
        return $this->expenseName;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function convertDateStringToDateTime()
    {
        $date = $this->getDueDate();

        if (is_string($date)) {
            $this->dueDate = new \DateTime($date);
        }
    }

}
