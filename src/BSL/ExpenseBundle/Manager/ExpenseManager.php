<?php
// src/BSL/ExpenseBundle/Manager/ExpenseManager.php
namespace BSL\ExpenseBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Class ExpenseManager
 * @package BSL\ExpenseBundle\Manager
 */
class ExpenseManager
{
    protected $em; //Entity manager
    protected $repo; // Repository
    protected $class; // Repository class name

    public function __construct(EntityManager $em, $class)
    {
        $this->em = $em;
        $this->class = $class;
        $this->repo = $em->getRepository($class);
    }

    public function get($id)
    {
        return $this->repo->findById($id);
    }
}