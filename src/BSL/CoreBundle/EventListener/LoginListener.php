<?php
// src/BSL/CoreBundle/EventListener/LoginListener.php

namespace BSL\CoreBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
* Listener responsible to change the redirection at the end of the password resetting
*/
class LoginListener implements EventSubscriberInterface
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
    * {@inheritDoc}
    */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationCompleted',
        );
    }

    public function onRegistrationCompleted(GetResponseUserEvent $event)
    {
        $url = $this->router->generate('BSLExpenseBundle_home');

        $event->setResponse(new RedirectResponse($url));
    }
}