<?php

namespace BSL\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BSLCoreBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
