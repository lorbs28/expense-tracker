// src/BSL/ExpenseBundle/Resources/public/js/views/ExpenseHomeView.js

/**
* ExpenseHomeView is a view model for the user homepage.
**/

var ExpenseHomeView = function() {
    var self = this;

    /******
     * Declare JS variables
     ******/
    // TODO: TEST, DELETE
    var counter = 0;


    /******
     * Declare observables
     ******/
    self.expenseName = ko.observable("");
    self.dueDate = ko.observable();
    self.amountDue = ko.observable();
    self.isEditingRecord = ko.observable();
    self.expenseId = ko.observable();
    self.isPaid = ko.observable();
    self.amountPaid = ko.observable();

    self.errors = ko.validation.group(self);

    /******
     * Declare observable arrays
     ******/
    self.payments = ko.observable();

    // used in the foreach binding to iterate through the expense payments
//    self.expenses = ko.observableArray([]);

    self.expenses = ko.observableArray(ko.utils.arrayMap(self.expenses, function(expense) {
        return {
            expenseName: expense.expenseName(),
            dueDate: expense.dueDate(),
            amountDue: expense.amountDue(),
            isEditingRecord: expense.isEditingRecord(),
            expenseId: expense.expenseId(),
            isPaid: expense.payment().isPaid(),
            amountPaid: expense.payment().amountPaid()
        }
    }));

    self.paidExpenses = ko.observableArray();

    /******
     * Declare computed observables
     ******/


    /******
     * Declare functions
     ******/
    // Function for added a new expense payment
    self.newExpense = function() {
        // Instantiate new object of ExpensePaymentModel where we'll be storing our information for each expense payment
        var expense = new ExpenseModel();
        var payment = new PaymentModel();

        counter++; // TODO: TEST, DELETE

        // TODO: TEST, DELETE THE VALUES
        expense.expenseName("American Express");
        expense.dueDate("04/19/2014");
        expense.amountDue(0);
        expense.expenseId(null);
        expense.isNew(true);
        expense.isEditingRecord(true);

        payment.amountPaid(0);
        payment.isPaid(false);
        payment.paymentDate("");

        expense.payment(payment);

        self.expenses.push(expense);
    };

    // Function to determine if the button to edit the record has been pressed.
    self.editRecord = function(item) {
        if (!item.isEditingRecord()) {
            item.isEditingRecord(true);
        } else {
            item.isEditingRecord(false);
        }
    };

    // Function for removing the selected expense payment
    self.removeExpense = function(expense) {

        if (!expense.isNew()) {
            expense.deleteExpense();
        }

        self.expenses.remove(expense);
    };

    // Function for saving the selected expense payment
    self.saveExpense = function(expense) {
        // Validate input
        if (self.errors().length == 0) {
            expense.isEditingRecord(false);

            if (expense.isNew()) {
                expense.saveNewExpense();
            } else {
                expense.updateExpense();
            }

        } else {
            self.errors.showAllMessages();
            console.log(self.errors().length);
        }

    };

    // Function for checking which payments have been paid.
    self.paid = function(expense) {
            if (expense.payment().isPaid()) {
                self.paidExpenses.push(expense);
                return true;
            } else {
                self.paidExpenses.remove(expense);
                return true;
            }

    };
};

$(document).ready(function() {
    var viewModel = new ExpenseHomeView();
    var expenseObj = new ExpenseModel();
    var expenses = expenseObj.getAllExpensesForUser();

    // We need to load any existing data for this page first.
    for (var expense in expenses) {
        viewModel.expenses.push(expense);
    }

    ko.applyBindings(viewModel);
});