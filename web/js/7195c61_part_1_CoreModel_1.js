var CoreModel = function() {
    var self = this;

    /**
     * Configure the Knockout validation plugin
     */
    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });
};