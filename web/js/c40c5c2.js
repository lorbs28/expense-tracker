// src/BSL/ExpenseBundle/Resources/public/js/views/ExpensePaymentsHomeView.js

/**
 * UserHomeView is a view model for the user homepage.
 **/

var ExpensePaymentsHomeView = function() {
    var self = this;

    // Declare JS variables;
    self.isEditing = false;

    // Declare observables
    // TODO: TEST, DELETE
    self.expenseName = ko.observable("Chase -3102");
    self.dueDate = ko.observable("06/28/2014");
    self.amountDue = ko.observable("99.99");
    self.amountPaid = ko.observable("0.00");

    self.paid = ko.observable();
    self.isPaid = ko.observable(false);
    self.isEditingRecord = ko.observable(false);

    // Declare computed observables

    /**
     * Determine if the button to edit the record has been pressed.
     * @type {*}
     */
    self.editRecord = ko.computed({
        read: function() {
            return false;
        },
        write: function (value) {

            if (!self.isEditing) {
                self.isEditing = true;
                self.isEditingRecord(true);
            } else {
                self.isEditing = false;
                self.isEditingRecord(false);
            }
        }
    }, this);

    // Declare functions

}

$(document).ready(function() {
    var viewModel = new ExpensePaymentsHomeView();
    ko.applyBindings(viewModel);
});