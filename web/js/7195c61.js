var CoreModel = function() {
    var self = this;

    /**
     * Configure the Knockout validation plugin
     */
    ko.validation.configure({
        registerExtenders: true,
        messagesOnModified: true,
        insertMessages: true,
        parseInputAttributes: true,
        messageTemplate: null
    });
};
/* =========================================================
 * bootstrap-datepicker.js 
 * http://www.eyecon.ro/bootstrap-datepicker
 * =========================================================
 * Copyright 2012 Stefan Petre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */
 
!function( $ ) {
	
	// Picker object
	
	var Datepicker = function(element, options){
		this.element = $(element);
		this.format = DPGlobal.parseFormat(options.format||this.element.data('date-format')||'mm/dd/yyyy');
		this.picker = $(DPGlobal.template)
							.appendTo('body')
							.on({
								click: $.proxy(this.click, this)//,
								//mousedown: $.proxy(this.mousedown, this)
							});
		this.isInput = this.element.is('input');
		this.component = this.element.is('.date') ? this.element.find('.add-on') : false;
		
		if (this.isInput) {
			this.element.on({
				focus: $.proxy(this.show, this),
				//blur: $.proxy(this.hide, this),
				keyup: $.proxy(this.update, this)
			});
		} else {
			if (this.component){
				this.component.on('click', $.proxy(this.show, this));
			} else {
				this.element.on('click', $.proxy(this.show, this));
			}
		}
	
		this.minViewMode = options.minViewMode||this.element.data('date-minviewmode')||0;
		if (typeof this.minViewMode === 'string') {
			switch (this.minViewMode) {
				case 'months':
					this.minViewMode = 1;
					break;
				case 'years':
					this.minViewMode = 2;
					break;
				default:
					this.minViewMode = 0;
					break;
			}
		}
		this.viewMode = options.viewMode||this.element.data('date-viewmode')||0;
		if (typeof this.viewMode === 'string') {
			switch (this.viewMode) {
				case 'months':
					this.viewMode = 1;
					break;
				case 'years':
					this.viewMode = 2;
					break;
				default:
					this.viewMode = 0;
					break;
			}
		}
		this.startViewMode = this.viewMode;
		this.weekStart = options.weekStart||this.element.data('date-weekstart')||0;
		this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
		this.onRender = options.onRender;
		this.fillDow();
		this.fillMonths();
		this.update();
		this.showMode();
	};
	
	Datepicker.prototype = {
		constructor: Datepicker,
		
		show: function(e) {
			this.picker.show();
			this.height = this.component ? this.component.outerHeight() : this.element.outerHeight();
			this.place();
			$(window).on('resize', $.proxy(this.place, this));
			if (e ) {
				e.stopPropagation();
				e.preventDefault();
			}
			if (!this.isInput) {
			}
			var that = this;
			$(document).on('mousedown', function(ev){
				if ($(ev.target).closest('.datepicker').length == 0) {
					that.hide();
				}
			});
			this.element.trigger({
				type: 'show',
				date: this.date
			});
		},
		
		hide: function(){
			this.picker.hide();
			$(window).off('resize', this.place);
			this.viewMode = this.startViewMode;
			this.showMode();
			if (!this.isInput) {
				$(document).off('mousedown', this.hide);
			}
			//this.set();
			this.element.trigger({
				type: 'hide',
				date: this.date
			});
		},
		
		set: function() {
			var formated = DPGlobal.formatDate(this.date, this.format);
			if (!this.isInput) {
				if (this.component){
					this.element.find('input').prop('value', formated);
				}
				this.element.data('date', formated);
			} else {
				this.element.prop('value', formated);
			}
		},
		
		setValue: function(newDate) {
			if (typeof newDate === 'string') {
				this.date = DPGlobal.parseDate(newDate, this.format);
			} else {
				this.date = new Date(newDate);
			}
			this.set();
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		place: function(){
			var offset = this.component ? this.component.offset() : this.element.offset();
			this.picker.css({
				top: offset.top + this.height,
				left: offset.left
			});
		},
		
		update: function(newDate){
			this.date = DPGlobal.parseDate(
				typeof newDate === 'string' ? newDate : (this.isInput ? this.element.prop('value') : this.element.data('date')),
				this.format
			);
			this.viewDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1, 0, 0, 0, 0);
			this.fill();
		},
		
		fillDow: function(){
			var dowCnt = this.weekStart;
			var html = '<tr>';
			while (dowCnt < this.weekStart + 7) {
				html += '<th class="dow">'+DPGlobal.dates.daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
		},
		
		fillMonths: function(){
			var html = '';
			var i = 0
			while (i < 12) {
				html += '<span class="month">'+DPGlobal.dates.monthsShort[i++]+'</span>';
			}
			this.picker.find('.datepicker-months td').append(html);
		},
		
		fill: function() {
			var d = new Date(this.viewDate),
				year = d.getFullYear(),
				month = d.getMonth(),
				currentDate = this.date.valueOf();
			this.picker.find('.datepicker-days th:eq(1)')
						.text(DPGlobal.dates.months[month]+' '+year);
			var prevMonth = new Date(year, month-1, 28,0,0,0,0),
				day = DPGlobal.getDaysInMonth(prevMonth.getFullYear(), prevMonth.getMonth());
			prevMonth.setDate(day);
			prevMonth.setDate(day - (prevMonth.getDay() - this.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			nextMonth.setDate(nextMonth.getDate() + 42);
			nextMonth = nextMonth.valueOf();
			var html = [];
			var clsName,
				prevY,
				prevM;
			while(prevMonth.valueOf() < nextMonth) {
				if (prevMonth.getDay() === this.weekStart) {
					html.push('<tr>');
				}
				clsName = this.onRender(prevMonth);
				prevY = prevMonth.getFullYear();
				prevM = prevMonth.getMonth();
				if ((prevM < month &&  prevY === year) ||  prevY < year) {
					clsName += ' old';
				} else if ((prevM > month && prevY === year) || prevY > year) {
					clsName += ' new';
				}
				if (prevMonth.valueOf() === currentDate) {
					clsName += ' active';
				}
				html.push('<td class="day '+clsName+'">'+prevMonth.getDate() + '</td>');
				if (prevMonth.getDay() === this.weekEnd) {
					html.push('</tr>');
				}
				prevMonth.setDate(prevMonth.getDate()+1);
			}
			this.picker.find('.datepicker-days tbody').empty().append(html.join(''));
			var currentYear = this.date.getFullYear();
			
			var months = this.picker.find('.datepicker-months')
						.find('th:eq(1)')
							.text(year)
							.end()
						.find('span').removeClass('active');
			if (currentYear === year) {
				months.eq(this.date.getMonth()).addClass('active');
			}
			
			html = '';
			year = parseInt(year/10, 10) * 10;
			var yearCont = this.picker.find('.datepicker-years')
								.find('th:eq(1)')
									.text(year + '-' + (year + 9))
									.end()
								.find('td');
			year -= 1;
			for (var i = -1; i < 11; i++) {
				html += '<span class="year'+(i === -1 || i === 10 ? ' old' : '')+(currentYear === year ? ' active' : '')+'">'+year+'</span>';
				year += 1;
			}
			yearCont.html(html);
		},
		
		click: function(e) {
			e.stopPropagation();
			e.preventDefault();
			var target = $(e.target).closest('span, td, th');
			if (target.length === 1) {
				switch(target[0].nodeName.toLowerCase()) {
					case 'th':
						switch(target[0].className) {
							case 'switch':
								this.showMode(1);
								break;
							case 'prev':
							case 'next':
								this.viewDate['set'+DPGlobal.modes[this.viewMode].navFnc].call(
									this.viewDate,
									this.viewDate['get'+DPGlobal.modes[this.viewMode].navFnc].call(this.viewDate) + 
									DPGlobal.modes[this.viewMode].navStep * (target[0].className === 'prev' ? -1 : 1)
								);
								this.fill();
								this.set();
								break;
						}
						break;
					case 'span':
						if (target.is('.month')) {
							var month = target.parent().find('span').index(target);
							this.viewDate.setMonth(month);
						} else {
							var year = parseInt(target.text(), 10)||0;
							this.viewDate.setFullYear(year);
						}
						if (this.viewMode !== 0) {
							this.date = new Date(this.viewDate);
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						this.showMode(-1);
						this.fill();
						this.set();
						break;
					case 'td':
						if (target.is('.day') && !target.is('.disabled')){
							var day = parseInt(target.text(), 10)||1;
							var month = this.viewDate.getMonth();
							if (target.is('.old')) {
								month -= 1;
							} else if (target.is('.new')) {
								month += 1;
							}
							var year = this.viewDate.getFullYear();
							this.date = new Date(year, month, day,0,0,0,0);
							this.viewDate = new Date(year, month, Math.min(28, day),0,0,0,0);
							this.fill();
							this.set();
							this.element.trigger({
								type: 'changeDate',
								date: this.date,
								viewMode: DPGlobal.modes[this.viewMode].clsName
							});
						}
						break;
				}
			}
		},
		
		mousedown: function(e){
			e.stopPropagation();
			e.preventDefault();
		},
		
		showMode: function(dir) {
			if (dir) {
				this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
			}
			this.picker.find('>div').hide().filter('.datepicker-'+DPGlobal.modes[this.viewMode].clsName).show();
		}
	};
	
	$.fn.datepicker = function ( option, val ) {
		return this.each(function () {
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('datepicker', (data = new Datepicker(this, $.extend({}, $.fn.datepicker.defaults,options))));
			}
			if (typeof option === 'string') data[option](val);
		});
	};

	$.fn.datepicker.defaults = {
		onRender: function(date) {
			return '';
		}
	};
	$.fn.datepicker.Constructor = Datepicker;
	
	var DPGlobal = {
		modes: [
			{
				clsName: 'days',
				navFnc: 'Month',
				navStep: 1
			},
			{
				clsName: 'months',
				navFnc: 'FullYear',
				navStep: 1
			},
			{
				clsName: 'years',
				navFnc: 'FullYear',
				navStep: 10
		}],
		dates:{
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		},
		isLeapYear: function (year) {
			return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))
		},
		getDaysInMonth: function (year, month) {
			return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
		},
		parseFormat: function(format){
			var separator = format.match(/[.\/\-\s].*?/),
				parts = format.split(/\W+/);
			if (!separator || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separator: separator, parts: parts};
		},
		parseDate: function(date, format) {
			var parts = date.split(format.separator),
				date = new Date(),
				val;
			date.setHours(0);
			date.setMinutes(0);
			date.setSeconds(0);
			date.setMilliseconds(0);
			if (parts.length === format.parts.length) {
				var year = date.getFullYear(), day = date.getDate(), month = date.getMonth();
				for (var i=0, cnt = format.parts.length; i < cnt; i++) {
					val = parseInt(parts[i], 10)||1;
					switch(format.parts[i]) {
						case 'dd':
						case 'd':
							day = val;
							date.setDate(val);
							break;
						case 'mm':
						case 'm':
							month = val - 1;
							date.setMonth(val - 1);
							break;
						case 'yy':
							year = 2000 + val;
							date.setFullYear(2000 + val);
							break;
						case 'yyyy':
							year = val;
							date.setFullYear(val);
							break;
					}
				}
				date = new Date(year, month, day, 0 ,0 ,0);
			}
			return date;
		},
		formatDate: function(date, format){
			var val = {
				d: date.getDate(),
				m: date.getMonth() + 1,
				yy: date.getFullYear().toString().substring(2),
				yyyy: date.getFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			var date = [];
			for (var i=0, cnt = format.parts.length; i < cnt; i++) {
				date.push(val[format.parts[i]]);
			}
			return date.join(format.separator);
		},
		headTemplate: '<thead>'+
							'<tr>'+
								'<th class="prev">&lsaquo;</th>'+
								'<th colspan="5" class="switch"></th>'+
								'<th class="next">&rsaquo;</th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
	};
	DPGlobal.template = '<div class="datepicker dropdown-menu">'+
							'<div class="datepicker-days">'+
								'<table class=" table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
								'</table>'+
							'</div>'+
						'</div>';

}( window.jQuery );
(function(){function o(a,b,c){a.isValidating(!0);b.validator(a(),c.params||!0,function(e){var d=!1,i="";if(a.__valid__()&&(e.message?(d=e.isValid,i=e.message):d=e,!d))a.error=ko.validation.formatMessage(i||c.message||b.message,c.params),a.__valid__(d);a.isValidating(!1)})}if(void 0===typeof ko)throw"Knockout is required, please ensure it is loaded before loading this validation plug-in";var j={registerExtenders:!0,messagesOnModified:!0,messageTemplate:null,insertMessages:!0,parseInputAttributes:!1,
writeInputAttributes:!1,decorateElement:!1,errorClass:null,errorElementClass:"validationElement",errorMessageClass:"validationMessage",grouping:{deep:!1,observable:!0}},f=ko.utils.extend({},j),k=["required","pattern","min","max","step"],d,l=(new Date).getTime(),m={};d={isArray:function(a){return a.isArray||"[object Array]"===Object.prototype.toString.call(a)},isObject:function(a){return null!==a&&"object"===typeof a},values:function(a){var b=[],c;for(c in a)a.hasOwnProperty(c)&&b.push(a[c]);return b},
getValue:function(a){return"function"===typeof a?a():a},hasAttribute:function(a,b){return null!==a.getAttribute(b)},isValidatable:function(a){return a.rules&&a.isValid&&a.isModified},insertAfter:function(a,b){a.parentNode.insertBefore(b,a.nextSibling)},newId:function(){return l+=1},getConfigOptions:function(a){return d.contextFor(a)||f},setDomData:function(a,b){var c=a.__ko_validation__;c||(a.__ko_validation__=c=d.newId());m[c]=b},getDomData:function(a){a=a.__ko_validation__;return!a?void 0:m[a]},
contextFor:function(a){switch(a.nodeType){case 1:case 8:var b=d.getDomData(a);if(b)return b;if(a.parentNode)return d.contextFor(a.parentNode)}},isEmptyVal:function(a){if(void 0===a||null===a||""===a)return!0}};var n=0;ko.validation={utils:d,init:function(a,b){if(!(0<n)||b)a=a||{},a.errorElementClass=a.errorElementClass||a.errorClass||f.errorElementClass,a.errorMessageClass=a.errorMessageClass||a.errorClass||f.errorMessageClass,ko.utils.extend(f,a),f.registerExtenders&&ko.validation.registerExtenders(),
n=1},configure:function(a){ko.validation.init(a)},reset:function(){f=$.extend(f,j)},group:function(a,b){var b=ko.utils.extend(f.grouping,b),c=ko.observableArray([]),e=null,g=function h(a,e){var g=[],f=ko.utils.unwrapObservable(a),e=void 0!==e?e:b.deep?1:-1;ko.isObservable(a)&&(a.isValid||a.extend({validatable:!0}),c.push(a));f&&(d.isArray(f)?g=f:d.isObject(f)&&(g=d.values(f)));0!==e&&ko.utils.arrayForEach(g,function(a){a&&!a.nodeType&&h(a,e+1)})};b.observable?(g(a),e=ko.computed(function(){var a=
[];ko.utils.arrayForEach(c(),function(b){b.isValid()||a.push(b.error)});return a})):e=function(){var b=[];c([]);g(a);ko.utils.arrayForEach(c(),function(a){a.isValid()||b.push(a.error)});return b};e.showAllMessages=function(a){a==void 0&&(a=true);e();ko.utils.arrayForEach(c(),function(b){b.isModified(a)})};a.errors=e;a.isValid=function(){return a.errors().length===0};a.isAnyMessageShown=function(){var a=false;e();ko.utils.arrayForEach(c(),function(b){!b.isValid()&&b.isModified()&&(a=true)});return a};
return e},formatMessage:function(a,b){return a.replace(/\{0\}/gi,b)},addRule:function(a,b){a.extend({validatable:!0});a.rules.push(b);return a},addAnonymousRule:function(a,b){var c=d.newId();void 0===b.message&&(rulesObj.message="Error");ko.validation.rules[c]=b;ko.validation.addRule(a,{rule:c,params:b.params})},addExtender:function(a){ko.extenders[a]=function(b,c){return c.message||c.onlyIf?ko.validation.addRule(b,{rule:a,message:c.message,params:d.isEmptyVal(c.params)?!0:c.params,condition:c.onlyIf}):
ko.validation.addRule(b,{rule:a,params:c})}},registerExtenders:function(){if(f.registerExtenders)for(var a in ko.validation.rules)ko.validation.rules.hasOwnProperty(a)&&(ko.extenders[a]||ko.validation.addExtender(a))},insertValidationMessage:function(a){var b=document.createElement("SPAN");b.className=d.getConfigOptions(a).errorMessageClass;d.insertAfter(a,b);return b},parseInputValidationAttributes:function(a,b){ko.utils.arrayForEach(k,function(c){d.hasAttribute(a,c)&&ko.validation.addRule(b(),{rule:c,
params:a.getAttribute(c)||!0})})},writeInputValidationAttributes:function(a,b){var c=b();if(c&&c.rules){var e=c.rules();ko.utils.arrayForEach(k,function(b){var c,d=ko.utils.arrayFirst(e,function(a){return a.rule.toLowerCase()===b.toLowerCase()});d&&(c=d.params,"pattern"==d.rule&&d.params instanceof RegExp&&(c=d.params.source),a.setAttribute(b,c))});e=null}}};ko.validation.rules={};ko.validation.rules.required={validator:function(a,b){var c=/^\s+|\s+$/g,e;if(void 0===a||null===a)return!b;e=a;"string"==
typeof a&&(e=a.replace(c,""));return b&&0<(e+"").length},message:"This field is required."};ko.validation.rules.min={validator:function(a,b){return d.isEmptyVal(a)||a>=b},message:"Please enter a value greater than or equal to {0}."};ko.validation.rules.max={validator:function(a,b){return d.isEmptyVal(a)||a<=b},message:"Please enter a value less than or equal to {0}."};ko.validation.rules.minLength={validator:function(a,b){return d.isEmptyVal(a)||a.length>=b},message:"Please enter at least {0} characters."};
ko.validation.rules.maxLength={validator:function(a,b){return d.isEmptyVal(a)||a.length<=b},message:"Please enter no more than {0} characters."};ko.validation.rules.pattern={validator:function(a,b){return d.isEmptyVal(a)||null!=a.match(b)},message:"Please check this value."};ko.validation.rules.step={validator:function(a,b){return d.isEmptyVal(a)||0===100*a%(100*b)},message:"The value must increment by {0}"};ko.validation.rules.email={validator:function(a,b){return d.isEmptyVal(a)||b&&/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(a)},
message:"Please enter a proper email address"};ko.validation.rules.date={validator:function(a,b){return d.isEmptyVal(a)||b&&!/Invalid|NaN/.test(new Date(a))},message:"Please enter a proper date"};ko.validation.rules.dateISO={validator:function(a,b){return d.isEmptyVal(a)||b&&/^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(a)},message:"Please enter a proper date"};ko.validation.rules.number={validator:function(a,b){return d.isEmptyVal(a)||b&&/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(a)},message:"Please enter a number"};
ko.validation.rules.digit={validator:function(a,b){return d.isEmptyVal(a)||b&&/^\d+$/.test(a)},message:"Please enter a digit"};ko.validation.rules.phoneUS={validator:function(a,b){if("string"!==typeof a)return!1;if(d.isEmptyVal(a))return!0;a=a.replace(/\s+/g,"");return b&&9<a.length&&a.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/)},message:"Please specify a valid phone number"};ko.validation.rules.equal={validator:function(a,b){return a===d.getValue(b)},message:"Values must equal"};
ko.validation.rules.notEqual={validator:function(a,b){return a!==d.getValue(b)},message:"Please choose another value."};ko.validation.rules.unique={validator:function(a,b){var c=d.getValue(b.collection),e=d.getValue(b.externalValue),g=0;if(!a||!c)return!0;ko.utils.arrayFilter(ko.utils.unwrapObservable(c),function(c){a===(b.valueAccessor?b.valueAccessor(c):c)&&g++});return g<(void 0!==e&&a!==e?1:2)},message:"Please make sure the value is unique."};ko.validation.registerExtenders();ko.bindingHandlers.validationCore=
{init:function(a,b){var c=d.getConfigOptions(a);if(c.parseInputAttributes){var e=function(){ko.validation.parseInputValidationAttributes(a,b)};window.setImmediate?window.setImmediate(e):window.setTimeout(e,0)}c.insertMessages&&d.isValidatable(b())&&(e=ko.validation.insertValidationMessage(a),c.messageTemplate?ko.renderTemplate(c.messageTemplate,{field:b()},null,e,"replaceNode"):ko.applyBindingsToNode(e,{validationMessage:b()}));c.writeInputAttributes&&d.isValidatable(b())&&ko.validation.writeInputValidationAttributes(a,
b);c.decorateElement&&d.isValidatable(b())&&ko.applyBindingsToNode(a,{validationElement:b()})},update:function(){}};var p=ko.bindingHandlers.value.init;ko.bindingHandlers.value.init=function(a,b,c,e,d){p(a,b,c);return ko.bindingHandlers.validationCore.init(a,b,c,e,d)};ko.bindingHandlers.validationMessage={update:function(a,b){var c=b(),e=d.getConfigOptions(a);ko.utils.unwrapObservable(c);var g=!1,f=!1;c.extend({validatable:!0});g=c.isModified();f=c.isValid();ko.bindingHandlers.text.update(a,function(){return!e.messagesOnModified||
g?f?null:c.error:null});ko.bindingHandlers.visible.update(a,function(){return g?!f:!1})}};ko.bindingHandlers.validationElement={update:function(a,b){var c=b(),e=d.getConfigOptions(a);ko.utils.unwrapObservable(c);var g=!1,f=!1;c.extend({validatable:!0});g=c.isModified();f=c.isValid();ko.bindingHandlers.css.update(a,function(){var a={},b=g?!f:!1;e.decorateElement||(b=!1);a[e.errorElementClass]=b;return a})}};ko.bindingHandlers.validationOptions={init:function(a,b){var c=ko.utils.unwrapObservable(b());
if(c){var e=ko.utils.extend({},f);ko.utils.extend(e,c);d.setDomData(a,e)}}};ko.extenders.validation=function(a,b){ko.utils.arrayForEach(d.isArray(b)?b:[b],function(b){ko.validation.addAnonymousRule(a,b)});return a};ko.extenders.validatable=function(a,b){if(b&&!d.isValidatable(a)){a.error=null;a.rules=ko.observableArray();a.isValidating=ko.observable(!1);a.__valid__=ko.observable(!0);a.isModified=ko.observable(!1);var c=ko.computed(function(){a();a.rules();ko.validation.validateObservable(a);return!0});
a.isValid=ko.computed(function(){return a.__valid__()});var e=a.subscribe(function(){a.isModified(!0)});a._disposeValidation=function(){a.isValid.dispose();a.rules.removeAll();a.isModified._subscriptions.change=[];a.isValidating._subscriptions.change=[];a.__valid__._subscriptions.change=[];e.dispose();c.dispose();delete a.rules;delete a.error;delete a.isValid;delete a.isValidating;delete a.__valid__;delete a.isModified}}else!1===b&&d.isValidatable(a)&&a._disposeValidation&&a._disposeValidation();
return a};ko.validation.validateObservable=function(a){for(var b=0,c,e,d=a.rules(),f=d.length;b<f;b++)if(e=d[b],!e.condition||e.condition())if(c=ko.validation.rules[e.rule],c.async||e.async)o(a,c,e);else{var h;h=a;c.validator(h(),void 0===e.params?!0:e.params)?h=!0:(h.error=ko.validation.formatMessage(e.message||c.message,e.params),h.__valid__(!1),h=!1);if(!h)return!1}a.error=null;a.__valid__(!0);return!0};ko.validatedObservable=function(a){if(!ko.validation.utils.isObject(a))return ko.observable(a).extend({validatable:!0});
var b=ko.observable(a);b.errors=ko.validation.group(a);b.isValid=ko.computed(function(){return 0===b.errors().length});return b};ko.validation.localize=function(a){for(var b in a)ko.validation.rules.hasOwnProperty(b)&&(ko.validation.rules[b].message=a[b])};ko.applyBindingsWithValidation=function(a,b,c){var e=arguments.length,d,f;2<e?(d=b,f=c):2>e?d=document.body:arguments[1].nodeType?d=b:f=arguments[1];ko.validation.init();f&&ko.validation.utils.setDomData(d,f);ko.applyBindings(a,b)};var q=ko.applyBindings;
ko.applyBindings=function(a,b){ko.validation.init();q(a,b)}})();
// src/BSL/ExpenseBundle/Resources/public/js/views/ExpenseModel.js

/**
 * ExpenseModel is a model for the business objects pertaining to expense.
 **/

var ExpenseModel = function() {
    var self = this;

    /******
     * Declare JS variables;
     ******/


    /******
     * Declare observables
     ******/
    self.expenseName = ko.observable("");
    self.dueDate = ko.observable("");
    self.amountDue = ko.observable("");
    self.expenseId = ko.observable("");
    self.isNew = ko.observable(""); // default to new expense payment
    self.isEditingRecord = ko.observable("");
    self.payment = ko.observable();

    /******
     * Declare computed observables
     ******/

    /******
     * Declare functions
     ******/
    //Save a new expense to the database
    self.saveNewExpense = function() {
        var route;
        var data;

        self.isNew(false); // Set isNew to false, since we're saving the new expense it will no longer be new after the save.
        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_createExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                dataType: "JSON",
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE

                    self.expenseId(result.expenseId);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    };

    // Update an existing expense
    self.updateExpense = function() {
        var route;
        var data;

        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_updateExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                dateType: "JSON",
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    };

    // Remove an expense
    self.deleteExpense = function() {
        var route;
        var data;

        data = ko.toJSON(self);

        route = Routing.generate("BSLExpenseBundle_deleteExpense");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    }

    // Get all expenses for user
    self.getAllExpensesForUser = function() {
        var route;

        route = Routing.generate("BSLExpenseBundle_getAllExpensesForUser");

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                success: function(result) {
                    console.log(result); // TODO DELETE
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText); // TODO REFACTOR
                }
            })
        }
    }


}

// src/BSL/ExpenseBundle/Resources/public/js/views/PaymentModel.js
/**
 * PaymentModel is a model for the business objects pertaining to expense.
 **/

var PaymentModel = function() {
    var self = this;

    self.amountPaid = ko.observable("");
    self.isPaid = ko.observable("");
    self.paymentDate = ko.observable("");
}