// src/BSL/ExpenseBundle/Resources/public/js/views/ExpensePaymentModel.js

/**
 * ExpensePaymentModel is a model for the business objects pertaining to expense.
 **/

var ExpensePaymentModel = function() {
    var self = this;

    /******
     * Declare JS variables;
     ******/


    /******
     * Declare observables
     ******/
    self.expenseName = ko.observable("");
    self.dueDate = ko.observable("");
    self.amountDue = ko.observable("");
    self.amountPaid = ko.observable("");
    self.isPaid = ko.observable("");
    self.expensePaymentId = ko.observable("");
    self.isNew = ko.observable(""); // default to new expense payment
    self.isEditingRecord = ko.observable("");

    /******
     * Declare computed observables
     ******/

    /******
     * Declare functions
     ******/
    //Save the expense payment
    self.saveExpensePayment = function() {
        var route;
        var data = ko.toJSON(self);

        if (self.isNew) {
            route = Routing.generate("BSLExpenseBundle_createExpense");
        } else {
            route = Routing.generate("BSLExpenseBundle_updateExpense");
        }

        console.log(data); //TODO DELETE

        if (route) {
            $.ajax({
                type: "POST",
                url: route,
                data: { jsonObj : data },
                success: function(result) {
                    console.log(result);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + XMLHttpRequest.responseText);
                }
            }).done(function(msg) {

            })
        }
    };

}
