// src/BSL/ExpenseBundle/Resources/public/js/views/PaymentModel.js

/**
 * PaymentModel is a model for the business objects pertaining to the payments for expenses.
 **/

var PaymentModel = function() {
    var self = this;

    /******
     * Declare JS variables;
     ******/


    /******
     * Declare observables
     ******/
    self.isPaid = ko.observable();
    self.amountPaid = ko.observable();

    /******
     * Declare computed observables
     ******/

    /******
     * Declare functions
     ******/
}