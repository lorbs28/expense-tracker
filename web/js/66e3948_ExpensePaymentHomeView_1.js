// src/BSL/ExpenseBundle/Resources/public/js/views/ExpensePaymentsHomeView.js

/**
* ExpenseHomeView is a view model for the user homepage.
**/

var ExpensePaymentHomeView = function() {
    var self = this;

    /******
     * Declare JS variables
     ******/
    // TODO: TEST, DELETE
    var counter = 0;


    /******
     * Declare observables
     ******/
    self.expenseName = ko.observable("").extend({
        required: {
            message: "Name of expense is required."
        },
        minLength: 3
    });
    self.dueDate = ko.observable();
    self.amountDue = ko.observable();
    self.amountPaid = ko.observable();
    self.isEditingRecord = ko.observable();
    self.expensePaymentId = ko.observable();
    self.isPaid = ko.observable();

    self.errors = ko.validation.group(self);

    /******
     * Declare observable arrays
     ******/
    // used in the foreach binding to iterate through the expense payments
    self.expensePayments = ko.observableArray(ko.utils.arrayMap(self.expensePayments, function(expensePayment) {
        return {
            expenseName: expensePayment.expenseName(),
            dueDate: expensePayment.dueDate(),
            amountDue: expensePayment.amountDue(),
            amountPaid: expensePayment.amountPaid(),
            isPaid: expensePayment.isPaid(),
            isEditingRecord: expensePayment.isEditingRecord(),
            expensePaymentId: expensePayment.expensePaymentId()
        }
    }));

    self.paidExpenses = ko.observableArray();

    /******
     * Declare computed observables
     ******/


    /******
     * Declare functions
     ******/
    // Function for added a new expense payment
    self.newExpensePayment = function() {
        // Instantiate new object of ExpensePaymentModel where we'll be storing our information for each expense payment
        var expensePayment = new ExpensePaymentModel();

        counter++; // TODO: TEST, DELETE

        // TODO: TEST, DELETE THE VALUES
        expensePayment.expenseName(counter);
        expensePayment.dueDate("04/19/2014");
        expensePayment.amountDue(0);
        expensePayment.amountPaid(0);
        expensePayment.isPaid(false);
        expensePayment.expensePaymentId(counter);
        expensePayment.isNew(true);
        expensePayment.isEditingRecord(true);

        self.expensePayments.push(expensePayment);
    };

    // Function to determine if the button to edit the record has been pressed.
    self.editRecord = function(item) {
        if (!item.isEditingRecord()) {
            item.isEditingRecord(true);
        } else {
            item.isEditingRecord(false);
        }
    };

    // Function for removing the selected expense payment
    self.removeExpensePayment = function(expensePayment) {
        self.expensePayments.remove(expensePayment);
    };

    // Function for saving the selected expense payment
    self.saveExpensePayment = function(expensePayment) {
        // Validate input
        if(self.errors().length == 0) {
            expensePayment.isEditingRecord(false);
            expensePayment.saveExpensePayment();
        } else {
            self.errors.showAllMessages();
            console.log(self.errors().length);
        }

    };

    // Function for checking which payments have been paid.
    self.paid = function(expensePayment) {
        console.log(expensePayment.isPaid());
        if (expensePayment.isPaid()) {
            self.paidExpenses.push(expensePayment);
        } else {
            self.paidExpenses.remove(expensePayment);
        }

        return true;
    };
};

$(document).ready(function() {
    var viewModel = new ExpensePaymentHomeView();
    ko.applyBindings(viewModel);
});