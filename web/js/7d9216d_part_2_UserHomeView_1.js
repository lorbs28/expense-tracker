// src/BSL/ExpenseBundle/Resources/public/js/views/UserHomeView.js

/**
 * UserHomeView is a view model for the user homepage.
 **/

var UserHomeView = function() {
    var self = this;

    self.isPaid = ko.observable();

    self.isExpensePaid = ko.computed(function() {
        return true;
    });
}

$(document).ready(function() {
    var viewModel = new UserHomeView();

    ko.applyBindings(viewModel);
});