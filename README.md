# Project Expense Tracker (tentative name) 

---

## Description

This is a Symfony2 app using KnockoutJS and Bootstrap for the front-end.  The purpose of this application is to help keep track of what expenses the user has already paid.  Sometimes automating everything may not be a good idea because it can make you question yourself if you REALLY did pay that expense or not.

Future developments include 

* long-term/short-term debt payoff planning.
* dashboard with charts to let user know how far along they are with their debt payments.  
